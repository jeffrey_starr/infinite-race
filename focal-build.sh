#!/usr/bin/env bash

set -e

rustup toolchain install stable

rustup target add wasm32-unknown-unknown

cargo install wasm-pack || true

wasm-pack build --target web

cp -r pkg www
