use std::cmp::min;
use std::usize;
use wasm_bindgen::prelude::*;

/// A Stepper defines the logic for when a player decides to move forward, based on the current
/// time index. Per the rules of the game, the number of advances must match a distribution
/// (uniform distribution in the basic game) after some finite number of steps, which the
/// Stepper can determine.
pub trait Stepper {
    /// Return true if the player will attempt to move forward at time t
    fn advance(&self, t: usize) -> bool;
}

/// A ModularStepper iterates through a given sequence of advances. The period of the distribution
/// matches the length of the `steps` field, and time indices beyond the length are indexed by the
/// modulus of the length.
#[derive(Debug, PartialEq, Eq)]
struct ModularStepper {
    steps: Box<[bool]>,
}

/// Error enumerations for parsing data
#[derive(Debug, PartialEq, Eq)]
pub enum InputError {
    /// Input string contained an invalid character for the grammar. usize: position of first
    /// invalid character
    InvalidCharacter(usize),
    /// Input string did not obey the uniform distribution. Number of zeros found in string
    /// and number of ones found in string, respectively; they should equal.
    NonUniformDistribution(u32, u32),
}

impl ModularStepper {
    /// Create a ModularStepper based on a 'binary string' where a 1 character indicates
    /// advancement at the time index and 0 indicates lack of advancement. The number of 0s
    /// and 1s must match or a NonUniformDistribution error will be returned. If the string
    /// cannot be parsed, an InvalidCharacter error will be returned.
    pub fn from(bstr: &str) -> Result<ModularStepper, InputError> {
        let barray: Result<Vec<bool>, InputError> = bstr
            .chars()
            .enumerate()
            .map(|(idx, c)| match c {
                '0' => Ok(false),
                '1' => Ok(true),
                _ => Err(InputError::InvalidCharacter(idx)),
            })
            .collect();

        match barray {
            Err(foo) => Err(foo),
            Ok(arr) => {
                let counts = ModularStepper::count_bools(&arr);
                if counts.0 != counts.1 {
                    Err(InputError::NonUniformDistribution(counts.0, counts.1))
                } else {
                    Ok(ModularStepper {
                        steps: Box::from(arr),
                    })
                }
            }
        }
    }

    fn count_bools(vec: &[bool]) -> (u32, u32) {
        let mut zeros = 0u32;
        let mut ones = 0u32;

        for b in vec {
            if *b {
                ones = ones + 1;
            } else {
                zeros = zeros + 1;
            }
        }

        (zeros, ones)
    }
}

impl Stepper for ModularStepper {
    fn advance(&self, t: usize) -> bool {
        self.steps[t % self.steps.len()]
    }
}

/// Fill the p0_pos and p1_pos buffers (up to the minimum length) with
/// computed positions of player 0 and 1 respectively. For the index t, the
/// value in the buffer indicates the *initial* position prior to evaluating the move.
/// The initial entry will be for time=0 and will have value 0 for both players.
///
/// If either buffer is of size zero, this function will return immediately without changing
/// either buffer.
pub fn run(p0: &dyn Stepper, p1: &dyn Stepper, p0_pos: &mut [i32], p1_pos: &mut [i32]) -> () {
    let max_steps = min(p0_pos.len(), p1_pos.len());

    if max_steps == 0 {
        return;
    }

    // Races begin with both players at position 0
    p0_pos[0] = 0;
    p1_pos[0] = 0;

    for t in 1..max_steps {
        let last_p0 = p0_pos[t - 1];
        let last_p1 = p1_pos[t - 1];
        let next_p0 = last_p0 + i32::from(p0.advance(t - 1));
        let next_p1 = last_p1 + i32::from(p1.advance(t - 1));

        if next_p0 == next_p1 {
            // intersection of both players; one may be bumped
            if last_p0 == last_p1 {
                // both were in the same position, so no bump occurs
                p0_pos[t] = next_p0;
                p1_pos[t] = next_p1;
            } else if last_p0 < last_p1 {
                p0_pos[t] = next_p0;
                p1_pos[t] = next_p1 - 1;
            } else {
                p0_pos[t] = next_p0 - 1;
                p1_pos[t] = next_p1;
            }
        } else {
            p0_pos[t] = next_p0;
            p1_pos[t] = next_p1;
        }
    }
}

fn to_wasm_err(err: InputError) -> js_sys::Error {
    match err {
        InputError::InvalidCharacter(c) => {
            js_sys::Error::new(&format!("Invalid character at index {}", c))
        }
        InputError::NonUniformDistribution(a, b) => js_sys::Error::new(&format!(
            "Non-uniform distribution; count of zero {} != count of ones {}",
            a, b
        )),
    }
}

/// Run a race with two modular steppers, p0 and p1. Populate the results into the p0_pos and
/// p1_pos buffers.
///
/// p0 and p1 are initialized per `ModularStepper::from`.
#[wasm_bindgen]
pub fn run_modular(
    p0: &str,
    p1: &str,
    p0_pos: &mut [i32],
    p1_pos: &mut [i32],
) -> Result<(), js_sys::Error> {
    let p0_stepper = ModularStepper::from(p0).map_err(to_wasm_err)?;
    let p1_stepper = ModularStepper::from(p1).map_err(to_wasm_err)?;

    Ok(run(&p0_stepper, &p1_stepper, p0_pos, p1_pos))
}

#[cfg(test)]
mod tests {

    mod run {
        use crate::run;
        use crate::ModularStepper;

        fn example_p0() -> ModularStepper {
            ModularStepper::from("0101").unwrap()
        }

        fn example_p1() -> ModularStepper {
            ModularStepper::from("0011").unwrap()
        }

        #[test]
        fn initialize_as_zero() {
            let mut buffer0 = [2; 1];
            let mut buffer1 = [4; 1];
            run(&example_p0(), &example_p1(), &mut buffer0, &mut buffer1);
            assert_eq!(buffer0[0], 0);
            assert_eq!(buffer1[0], 0);
        }

        #[test]
        fn populate_four_turns_as_expected() {
            let mut buffer0 = [3; 5];
            let mut buffer1 = [3; 5];
            run(&example_p0(), &example_p1(), &mut buffer0, &mut buffer1);
            assert_eq!(buffer0, [0, 0, 1, 0, 1]);
            assert_eq!(buffer1, [0, 0, 0, 1, 2]);
        }

        #[test]
        fn populate_nine_turns_as_expected() {
            let mut buffer0 = [3; 9];
            let mut buffer1 = [3; 9];
            run(&example_p0(), &example_p1(), &mut buffer0, &mut buffer1);
            assert_eq!(buffer0, [0, 0, 1, 0, 1, 1, 2, 1, 2]);
            assert_eq!(buffer1, [0, 0, 0, 1, 2, 2, 1, 2, 3]);
        }
    }

    mod modular_stepper {
        use crate::{InputError, ModularStepper};

        #[test]
        fn from_empty_string() {
            let empty: [bool; 0] = [false; 0];

            assert_eq!(
                ModularStepper::from(""),
                Ok(ModularStepper {
                    steps: Box::new(empty)
                })
            );
        }

        #[test]
        fn from_valid_double_characters() {
            assert_eq!(
                ModularStepper::from("10"),
                Ok(ModularStepper {
                    steps: Box::new([true, false])
                })
            );
            assert_eq!(
                ModularStepper::from("01"),
                Ok(ModularStepper {
                    steps: Box::new([false, true])
                })
            );
        }

        #[test]
        fn from_valid_multiple_characters() {
            assert_eq!(
                ModularStepper::from("100110"),
                Ok(ModularStepper {
                    steps: Box::new([true, false, false, true, true, false])
                })
            );
        }

        #[test]
        fn from_invalid_characters() {
            assert_eq!(
                ModularStepper::from("120"),
                Err(InputError::InvalidCharacter(1))
            );
        }

        #[test]
        fn from_invalid_distibution() {
            assert_eq!(
                ModularStepper::from("00011"),
                Err(InputError::NonUniformDistribution(3, 2))
            );
        }
    }
}
