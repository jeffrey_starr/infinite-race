# Infinite Race

Library for simulating 'infinite races' between two players.

## Infinite Race

An infinite race is a two-player game. The board is the integer number line. Both players start at position 0.
Moves are taken simultaneously and a player either does not move forward or moves forward one position (in the
binary version of the game). If the two players move to the same location, one of the players may be
"bumped" --- moved backwards one position. If a player is coming from behind the other player (a lower initial
position), then that player bumps the opponent. For example, if player A is at position 1 and player B is at position
0, and player A does not move forward but player B moves forward, then they will coincide at the same location. Since
player B started at a further back position, player A will be bumped back to position 0 and player B will take
position 1. If both players were at the same position, then they may occupy the same position afterward (i.e. moving
in tandem does not cause bump backs).

Each player pre-determines their strategy by specifying what they will do at a specific time index. However, the strategy
must obey the distribution of moves for the game; for the binary version, this is a uniform distribution of zeros
and ones. Players have the freedom to specify over what period of time their moves will obey the distribution.

## API and Usage

### Specifying a player

(discuss Stepper and ModularStepper)

### Running a game

(discuss `run` method)

## Building

### Prerequisites

1. rust and cargo
2. wasm-pack (`cargo install wasm-pack`)

### Steps

Note: `wasm-pack` may be under `$HOME/.cargo/bin`.

1. `wasm-pack build --target web`
